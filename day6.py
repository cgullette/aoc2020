#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 6.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# Given a line iterator into the datalist, pull off all lines
# of a single group and return the number of questions answered
def getNumAnswers(lineIter) : 
    groupDict = dict()
    line = next(lineIter)
    while (len(line) > 0) :
        for c in line :
            groupDict[c] = 1
        line = next(lineIter)
    return len(groupDict)


# Given a line iterator into the datalist, pull off all lines
# of a single group and return the number of questions answered by
# all members of that group
def getNumCommonAnswers(lineIter) :
    groupDict = dict()
    line = next(lineIter)
    groupSize = 0

    while (len(line) > 0) :
        groupSize += 1
        for c in line :
            if c not in groupDict :
                groupDict[c] = 1
            else :
                temp = groupDict[c]
                groupDict[c] = temp + 1
        line = next(lineIter)

    count = 0
    for val in groupDict.values() :
        if val == groupSize :
            count += 1
    return count


# Call until we run out of file. Note that I "cheated" and added a blank line to the end
# of the data file in order to avoid special case handling for the last group
def countAnswers(day) :
    count = 0
    lineIter = iter(datalist)

    try :
        while True :
            if (day == "1") :
                count += getNumAnswers(lineIter)
            elif (day == "2") :
                count += getNumCommonAnswers(lineIter)
    except StopIteration as e :
        return count


# Common helper functions
def readData(day) :
    # data files should always be "dayX.txt"
    filename = "day{}.txt".format(day)
    #filename = "test{}.txt".format(day)

    # Open the file and store each line
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            datalist.append(line.strip())

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day6.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    print ("Day {}: Sum of group counts is {}.".format(args[1], countAnswers(args[1])))

# Main body
if __name__ == '__main__' :
    main()

