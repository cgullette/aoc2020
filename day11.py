#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 11.
"""

# Imports
import sys
import re
import pprint

# Global variables
datalist = []


# Iterate through one cycle of seat changes using the following rules:
# - Empty seats ('L') become occupied ('#') if none of the eight adjacent seats are occupied.
# - Occupied seats ('#') become empty ('L') if four or more adjacent seats are occupied.
# - In all other cases the seat state does not change.
# Assumes that datalist is a rectangular grid (a list of strings, all strings are the same length)
#
# Returns a tuple with (# seats occupied, total # seats, # of seats that changed state)
#
# NOTE: This changes the state of the global datalist!
def cycleSeatsPartOne() :
    global datalist
    seatsOccupied = 0
    totalSeats = 0
    seatsThatChanged = 0
    newSeating = []

    rows = len(datalist)
    cols = len(datalist[0])

    for rowIdx in range(len(datalist)) :
        newRow = datalist[rowIdx].copy()
        totalSeats += len(newRow)
        for colIdx in range(len(datalist[rowIdx])) :
            curChar = datalist[rowIdx][colIdx]
            # Get the number of adjacent occupied seats
            adjOccSeats = 0
            for r in range(rowIdx-1,rowIdx+2) :
                for c in range(colIdx-1,colIdx+2) :
                    if r > -1 and r < len(datalist) and c > -1 and c < len(datalist[rowIdx]) :
                        if datalist[r][c] == '#' :
                            adjOccSeats += 1
            #print("aoc for r {} c {} '{}' is {}".format(rowIdx, colIdx, datalist[rowIdx][colIdx], adjOccSeats))

            if curChar == 'L' : # empty
                if adjOccSeats == 0 :
                    newRow[colIdx] = '#'
                    seatsOccupied += 1
                    seatsThatChanged += 1
            elif curChar == '#' : # occupied
                # current seat is included in adjOccSeats so it's got to be at least 5
                if adjOccSeats > 4 :
                    newRow[colIdx] = 'L'
                    seatsThatChanged += 1
                else :
                    seatsOccupied += 1
        newSeating.append(newRow)

    datalist = newSeating
    return seatsOccupied, totalSeats, seatsThatChanged


# Helper to look in a given direction and return if an occupied seat is found
def lookForSeat(row, col, rDelta, cDelta) :
    while True : 
        row += rDelta
        col += cDelta
        if row > -1 and row < len(datalist) and col > -1 and col < len(datalist[row]) :
            if datalist[row][col] == '#' :
                return True
            elif datalist[row][col] == 'L' :
                return False
        else :
            return False
 
# Iterate through one cycle of seat changes using the following rules:
# For each seat, we're counting the first seat visible in each direction as "adjacent".
# This means skipping over empty (floor) tiles.
# Also, it now takes five or more adjacent occupied seats to flip occupied to empty.
# Otherwise, the rules are the same as for cycleSeatsPartOne.
#
# Returns a tuple with (# seats occupied, total # seats, # of seats that changed state)
#
# NOTE: This changes the state of the global datalist!
def cycleSeatsPartTwo() :
    global datalist
    seatsOccupied = 0
    totalSeats = 0
    seatsThatChanged = 0
    newSeating = []

    rows = len(datalist)
    cols = len(datalist[0])

    for rowIdx in range(len(datalist)) :
        newRow = datalist[rowIdx].copy()
        totalSeats += len(newRow)
        for colIdx in range(len(datalist[rowIdx])) :
            # Get the number of adjacent occupied seats
            adjOccSeats = 0

            # Look N
            if lookForSeat(rowIdx, colIdx, -1, 0) :
                adjOccSeats += 1
            # Look NE
            if lookForSeat(rowIdx, colIdx, -1, 1) :
                adjOccSeats += 1
            # Look E
            if lookForSeat(rowIdx, colIdx, 0, 1) :
                adjOccSeats += 1
            # Look SE
            if lookForSeat(rowIdx, colIdx, 1, 1) :
                adjOccSeats += 1
            # Look S
            if lookForSeat(rowIdx, colIdx, 1, 0) :
                adjOccSeats += 1
            # Look SW
            if lookForSeat(rowIdx, colIdx, 1, -1) :
                adjOccSeats += 1
            # Look W
            if lookForSeat(rowIdx, colIdx, 0, -1) :
                adjOccSeats += 1
            # Look NW
            if lookForSeat(rowIdx, colIdx, -1, -1) :
                adjOccSeats += 1

            curChar = datalist[rowIdx][colIdx]
            if curChar == 'L' : # empty
                if adjOccSeats == 0 :
                    newRow[colIdx] = '#'
                    seatsOccupied += 1
                    seatsThatChanged += 1
            elif curChar == '#' : # occupied
                if adjOccSeats > 4 :
                    newRow[colIdx] = 'L'
                    seatsThatChanged += 1
                else :
                    seatsOccupied += 1
        newSeating.append(newRow)

    datalist = newSeating
    return seatsOccupied, totalSeats, seatsThatChanged

 
# Read the seat map as a list of strings (rows). Note that 'L' denotes an empty seat,
# '.' denotes floor space, and '#' denotes an occupied seat.
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            # Store each line as a list of chars (not a string)
            temp = []
            for c in line.strip() :
                temp.append(c)
            datalist.append(temp)

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day11.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    if args[1] == "1" :
        changed = -1
        while changed != 0 :
            #pprint.pprint(datalist, width=100)
            occupied, total, changed = cycleSeatsPartOne()
            print ("{} of {} seats changed, {} are occupied".format(changed, total, occupied))
        print("Day 1 complete.")
    elif args[1] == "2" :
        changed = -1
        while changed != 0 :
            #pprint.pprint(datalist, width=100)
            occupied, total, changed = cycleSeatsPartTwo()
            print ("{} of {} seats changed, {} are occupied".format(changed, total, occupied))
        print("Day 2 complete.")

# Main body
if __name__ == '__main__' :
    main()

