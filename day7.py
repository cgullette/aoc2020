#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 7.
"""

# Imports
import sys
import re

# Global variables
datalist = []
bagDict = dict()


# This function takes a line from datalist and stores each "bag"
# in a dict. Bag description is the key (e.g. "faded blue") and
# the value is a list of tuples (count, bag description) for
# all contained bags. No error checking here, I'm assuming each
# line defines a new bag type.
def storeBagInfo(line) :
    lineIter = iter(line)
    bagName = next(lineIter)
    innerBags = []
    for item in lineIter :
        if (item == "no other") :
            innerBags.append((0, "no other"))
        else :
            bag = item.split(' ', 1)
            innerBags.append((int(bag[0]),bag[1]))
    #print("Key = {}, Value = {}".format(bagName, innerBags))
    bagDict[bagName] = innerBags


def containsShinyGold(name) :
    if name not in bagDict :
        print(name + " not found in dict")
        return False

    for innerBag in bagDict[name] :
        if innerBag[1] == "shiny gold" :
            return True
        elif innerBag[1] == "no other" :
            return False
        else :
            #print ("checking inner bag " + innerBag[1])
            if containsShinyGold(innerBag[1]) :
                return True
    return False


def getShinyGoldCount() :
    sgCount = 0
    for bagName in bagDict.keys() :
        print ("Checking outer bag " + bagName)
        if containsShinyGold(bagName) :
            sgCount += 1
    return sgCount


# returns the total number of bags contained by name (including itself)
def countNestedBags(name) :
    if name not in bagDict :
        return 0

    count = 0
    for innerBag in bagDict[name] :
        innerBagCount = countNestedBags(innerBag[1])
        count += innerBag[0] * innerBagCount

    # don't forget to count yourself
    return count + 1


# Read the data file and store each line as a partially parsed list of bags
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            temp = []
            for item in (i.strip('.').split(", ") for i in line.strip().split(" bags contain ")) :
                temp.append(item)
            # remove the inner lists and "bag/bags"
            temp = temp[0] + temp[1]
            thisline = []
            for item in temp :
                if " bags" in item :
                    thisline.append(item[:-5])
                elif " bag" in item :
                    thisline.append(item[:-4])
                else :
                    thisline.append(item)
            datalist.append(thisline)

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day7.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    for line in datalist :
        storeBagInfo(line)

    if args[1] == "1" :
        print ("Day 1: {} bags can contain a shiny gold bag.".format(getShinyGoldCount()))
    elif args[1] == "2" :
        print ("Day 2: A shiny gold bag contains {} bags not including itself.".format(countNestedBags("shiny gold") - 1))

# Main body
if __name__ == '__main__' :
    main()

