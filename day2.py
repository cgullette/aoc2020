#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 2.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# Instance functions for specific problems

# Common helper functions
def readData(day):
    # data files should always be "dayX.txt"
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f:
        for line in f:
            # Each line is formatted as follows:
            # x-y a: <password>
            # Where x and y are ints defining the max/min number of character a
            # that is allowed in the password string
            rawList = re.split(r'[- :]', line.rstrip())

            # Now make it usable
            cleanList = [int(rawList[0]), int(rawList[1]), rawList[2], rawList[4]]
            datalist.append(cleanList)

# The count of the given character must be in the range given
def charCount() :
    # counter for number of good passwords
    counter = 0

    for pw in datalist :
        # count instances of pw[2] in string pw[3]
        charCount = 0
        for c in pw[3] :
            if c == pw[2] :
                charCount += 1
    
        # check against range and increment counter if good
        if charCount in range(pw[0], pw[1]+1) :
            counter += 1

    print("Number of valid passwords(1) = {}".format(counter))

# One and only one instance of the character must exist at the given indices
# Note indices start at one, not zero
def charCheck() :
    counter = 0

    for pw in datalist : 
        check = 0
        if pw[0] <= len(pw[3]) and pw[3][pw[0]-1] == pw[2] :
            check += 1
        if pw[1] <= len(pw[3]) and pw[3][pw[1]-1] == pw[2] :
            check += 1
        if check == 1 : 
            counter += 1

    print("Number of valid passwords(2) = {}".format(counter))

def main():
    args = sys.argv[1:]

    if not args:
        print('usage: python3 day2.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    # CALL YOUR INSTANCE FUNCTION HERE
    if args[1] == "1":
        charCount()
    elif args[1] == "2":
        charCheck()

# Main body
if __name__ == '__main__':
    main()

