#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 14.
"""

# Imports
import sys
import re

# Global variables
datalist = []


# Build a bitmask for ones and a bitmask for zeros.
# bmOne will have ones for all required ones, zeroes otherwise
# bmZero will have zeroes for all required zeroes, ones otherwise
# To use these masks on a number n: newN = (n | bmOne) & bmZero
def getMasks(maskStr) :
    bmOne = 0
    bmZero = 0
    for c in maskStr :
        bmOne <<= 1
        bmZero <<= 1
        if c == '1' :
            bmOne += 1
        if c != '0' :
            bmZero += 1
    #print("bmOne is {}, bmZero is {}".format(bin(bmOne),bin(bmZero)))
    return bmZero, bmOne


# Day 1: Run the program then gather the sum of all non-zero memory values
def initAndSumMemory() :
    memory = dict()
    bmZero = 0
    bmOne = 0
    # iterate through each instruction
    for item in datalist :
        if item[0] == "mask" :
            bmZero, bmOne = getMasks(item[1])
        else :
            #print("memory[{}] = ({} | {}) & {}) = {}".format(
            #    item[0], bin(item[1]), bin(bmOne), bin(bmZero), (item[1] | bmOne) & bmZero))
            memory[item[0]] = (item[1] | bmOne) & bmZero
    # now print and sum each value in memory
    memorySum = 0
    for item in memory.values() :
        #print("found {} in memory".format(item))
        memorySum += item
    print("Day 1: sum of all remaining memory items is {}".format(memorySum))


# Recursive call to do memory assignments based on masks with floating bits.
# See comment for initAndSumMemoryV2 for more info on how the bitmasks work.
# This function will assign value to memory[addr] after applying the mask to
# the memory address.
def assignValueToMemory(memory, addr, mask, value) :
    firstX = mask.find("X")
    # Not found, so apply the mask and store
    if firstX == -1 :
        maskInt = int(mask, 2)
        #print("value {}, mask {}, maskInt {}, addr {}, maskInt|addr {}".format(
        #    value, mask, maskInt, addr, maskInt|addr))
        memory[maskInt|addr] = value
    else :
        # Replace the X bit in *addr and mask* with zero
        newMask = mask[:firstX] + '0' + mask[firstX+1:]
        addrStr = "{0:036b}".format(addr)
        addrStr = addrStr[:firstX] + '0' + addrStr[firstX+1:]
        assignValueToMemory(memory, int(addrStr, 2), newMask, value)
        # Replace the X bit in *addr and mask* with one
        newMask = mask[:firstX] + '1' + mask[firstX+1:]
        addrStr = addrStr[:firstX] + '1' + addrStr[firstX+1:]
        assignValueToMemory(memory, int(addrStr, 2), newMask, value)


# Day 2: Run the program then gather the sum of all non-zero memory values
# This time, each 'X' in the bitmask represents both possible values 0 and 1,
# and the bitmask is applied to the memory location as follows:
#   - bit 0 leaves the memory address bit unchanged
#   - bit 1 overwrites the memory address bit with 1
#   - bit X takes on both values 0 and 1
def initAndSumMemoryV2() :
    memory = dict()
    mask = ""
    # iterate through each instruction
    for item in datalist :
        if item[0] == "mask" :
            mask = item[1]
        else :
            assignValueToMemory(memory, item[0], mask, item[1])
    # now print and sum each value in memory
    memorySum = 0
    for item in memory.values() :
        memorySum += item
    print("Day 2: sum of all remaining memory items is {}".format(memorySum))


# Read the program (mask and mem sets) and store in datalist
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    #filename = "test{}b.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            # Each line is "mem[x] = value" or "mask = value"
            # Store these as a list
            thisline = line.strip().split(" = ")
            if thisline[0][0:3] == "mem" :
                thisline[0] = int(thisline[0][4:-1])
                thisline[1] = int(thisline[1])
            datalist.append(thisline)

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 dayX.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    # CALL YOUR INSTANCE FUNCTION HERE
    if args[1] == "1" :
        initAndSumMemory()
    elif args[1] == "2" :
        initAndSumMemoryV2()

# Main body
if __name__ == '__main__' :
    main()

