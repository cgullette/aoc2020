#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 9.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# After the preamble, find the first value that is not a sum of two previous values
def findFirstBadValue(preambleLength) :
    currentList = []

    for value in datalist :
        if len(currentList) < preambleLength :
            currentList.append(value)
        else :
            valid = False
            for val1 in datalist :
                for val2 in datalist :
                    if val1 + val2 == value and val1 != val2 :
                        valid = True
                        break
                if valid is True :
                    break
            if valid is True :
                currentList.pop(0)
                currentList.append(value)
            else :
                print ("Day 1: first invalid value is {}".format(value))
                return value
    print ("Day 1: no invalid value found!")
    return 0


# Using the result of findFirstBadValue, find a range of contiguous values that add
# up to that result. Return the sum of the smallest and largest numbers in this range
def findWeakness(invalidVal) :
    startIdx = 0
    endIdx = 1
    currentSum = datalist[startIdx]

    while endIdx < len(datalist) :
        currentSum += datalist[endIdx]
        if currentSum == invalidVal :
            # Get the sum of the smallest and largest values in the list
            subList = datalist[startIdx:endIdx+1]
            subList.sort()
            print("Day 2: sum of invalid range extents is {}".format(subList[0] + subList[-1]))
            return

        if currentSum > invalidVal :
            startIdx += 1
            endIdx = startIdx + 1
            currentSum = datalist[startIdx]
        elif currentSum < invalidVal :
            endIdx += 1

    print("Day 2: sum not found!")


# read today's input (one int per line)
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            datalist.append(int(line.strip()))

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day9.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    if args[1] == "1" :
        findFirstBadValue(25)
    elif args[1] == "2" :
        findWeakness(findFirstBadValue(25))

# Main body
if __name__ == '__main__' :
    main()

