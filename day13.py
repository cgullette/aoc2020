#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 13.
"""

# Imports
import sys
import re

# Global variables
datalist = []
timeOfDeparture = 0


# For each bus ID, determine the smallest multiple that is bigger than TOD.
# Whichever is closest to TOD is selected. Return the bus ID times the difference
# between actual departure time and desired time.
def findEarliestBus() :
    timeToWait = timeOfDeparture
    bestBusSoFar = 0

    for bus in datalist :
        temp = timeOfDeparture % bus[0]
        if temp == 0 :
            timeToWait = 0
            bestBusSoFar = bus[0]
            print("OOPS: Bus ID is {}, time to wait is 0 minutes, product is 0.".format(bus[0]))
            return
        temp = bus[0] - temp
        if temp < timeToWait :
            timeToWait = temp
            bestBusSoFar = bus[0]

    print("Day 1: Bus ID is {}, time to wait is {} minutes, product is {}.".format(
        bestBusSoFar,timeToWait,bestBusSoFar*timeToWait))


# Each bus ID has a corresponding timestamp offset. Find the earliest time T
# at which all busses can depart at T + offset. This brute force approach works
# on the test data but is too slow for the problem data.
def findEarliestSyncedTime() :
    time = 0
    while True :
        if (time % 1000000 == 0) :
            print("time = {}".format(time))
        datsAGoodOne = True
        for bus in datalist :
            temp = time % bus[0]
            if bus[1] == 0 :
                if temp != 0 :
                    datsAGoodOne = False
                    break
            elif temp != bus[0] - bus[1] :
                datsAGoodOne = False
                break
        if datsAGoodOne :
            print("Day 2: First time that works is {}".format(time))
            return
        time += 1


# Another try, but this time we use mods to shortcut. We'll iterate through the
# list, finding the smallest offset multiple of the next bus ID that also meets the
# requirements of the previous busses.
def fasterFindEarliestSyncedTime() :
    base = 1
    incrementer = 1
    for bus in datalist :
        index = 0
        newBase = 0
        newIncr = 0
        # Find the first multiple of base and incrementer that meet the offset requirements
        while newBase == 0 :
            temp = base + index * incrementer + bus[1]
            if temp % bus[0] == 0 :
                newBase = temp - bus[1]
            index += 1
        # Now find the next multiple, because the distance to the first instance is not
        # always the same as the distance from the first instance to the second
        while newIncr == 0 :
            temp = base + index * incrementer + bus[1]
            if temp % bus[0] == 0 :
                newIncr = temp - newBase - bus[1]
            index += 1
        base = newBase
        incrementer = newIncr
        #print("For bus {} ({}), base is {}, incr is {}".format(bus[0],bus[1],base,incrementer))
    print("Day 2: First time that works is {}".format(base))


# Read two-line data file. Line one is a single int (time of departure).
# Line two is a comma separated list of bus IDs and 'x' (line out of service).
def readData(day) :
    global timeOfDeparture

    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        # Read the first line and store as timeOfDeparture
        timeOfDeparture = int(f.readline().strip())
        print("TOD = {}".format(timeOfDeparture))
        # Read the second line and store as ID / time offset tuples
        tsOffset = 0
        for item in f.readline().strip().split(",") :
            try :
                datalist.append((int(item),tsOffset))
            except ValueError :
                pass
            tsOffset += 1
        print("Bus IDs/offsets = {}".format(datalist))

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 dayX.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    if args[1] == "1" :
        findEarliestBus()
    elif args[1] == "2" :
        fasterFindEarliestSyncedTime()

# Main body
if __name__ == '__main__' :
    main()

