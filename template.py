#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 1.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# Instance functions for specific problems

# Common helper functions
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            # Store each data element in list of ints
            thisline = []
            for item in line.strip().split(",") :
                thisline.append(int(item))
            datalist.append(thisline)

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 dayX.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    # CALL YOUR INSTANCE FUNCTION HERE
    if args[1] == "1" :
        print("1") # call part 1
    elif args[1] == "2" :
        print("2") # call part 2

# Main body
if __name__ == '__main__' :
    main()

