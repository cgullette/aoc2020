#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 10.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# walk down the list of adapter joltages, tracking differences of 1,2,3 jolts
def adapterChain() :
    diffs = [0, 0, 0]

    # datalist[0] is always 0 so we'll init currentJoltage and skip it
    currentJoltage = 0

    for i in datalist[1:] :
        #print("current = {}, i = {}".format(currentJoltage, i))
        diff = i - currentJoltage
        if diff in range(1,4) :
            diffs[diff-1] += 1
            currentJoltage = i
        else :
            print("BAD DIFF == {}".format(diff))
            return

    print ("Day 1: 1-jolt diffs * 3-jolt diffs == {} * {} == {}".format(diffs[0],diffs[2],diffs[0]*diffs[2]))


# Is the adapter list valid?
# (no jumps in the sequence smaller than 1 or larger than 3)?
# Note: assumes list is already sorted and chain is at least len 2
def isChainValid(chain) :
    prevValue = chain[0]
    for i in chain[1:] :
        if i - prevValue not in range(1,4) :
            return False
        prevValue = i
    return True


# Check the validity of chain and all subchains where one or more values in range(startIdx, endIdx)
# have been removed. Return the total number of all valid subchains.
# Note: This way works, but it's brute force and horribly inefficient. I was not able to get an
# answer on the final puzzle data because of this, so I had to rework my solution (see below).
def countValidSubchains(chain, startIdx, depth) :
    # check myself before I wreck myself
    if not isChainValid(chain) :
        return 0

    # we can never remove the last element (device) or next to last element
    # (biggest adapter), so we know we will never have to test subchains smaller than 3
    if len(chain) < 4 :
        return 1

    # for each element from idx to the end, remove a single element and check all subchains
    countSoFar = 1

    # don't bother testing subchains past the last three elements
    for i in range(startIdx, len(chain) - 3) :
        val = chain.pop(i+1)
        countSoFar += countValidSubchains(chain[startIdx:], 0, depth+1)
        chain.insert(i+1, val)

    return countSoFar


# I'm assuming that there are only 1-jolt and 3-jolt differences between adapters
# since the sample data never had any 2-jolt steps. For each chain of 1-jolt
# transitions, multiply by the number of new subchains that are created.
#
# I arrived at this empirically initially, but it looks like for a chain of n
# 1-jolt transitions, the total number of transitions is t(n) = t(n-1) + t(n-2) + t(n-3)
# where t(1) = 1 and t(n) = 0 for n < 1
def quickCountValid(chain) :
    # We assume that the entire chain is valid
    totalSubchains = 1

    scTracker = [0, 0, 0]
    for idx in range(len(chain) - 1) :
        diff = chain[idx+1] - chain[idx]
        if diff == 1 :
            if scTracker == [0, 0, 0] :
                scTracker = [0, 0, 1]
            else :
                temp = scTracker.pop(0)
                scTracker.append(temp + scTracker[0] + scTracker[1])
        elif diff == 3 :
            if scTracker[2] > 0 :
                totalSubchains *= scTracker[0] + scTracker[1] + scTracker[2]
            scTracker = [0, 0, 0]
        else :
            print ("CRAP. We have a diff of -2.")

    return totalSubchains


def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            # Each line is a single int
            datalist.append(int(line))

    # For today we want to sort, put 0 (charging outlet) and largest + 3 (device adapter)
    # on our list
    datalist.sort()
    datalist.insert(0, 0)
    datalist.append(datalist[-1] + 3)
    #print(datalist)


def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day10.py [day] [part]')
        sys.exit(1)

    readData(args[0])

    if args[1] == "1" :
        adapterChain()
    elif args[1] == "2" :
        #print ("Day 2: total distinct arrangements = {}".format(countValidSubchains(datalist, 0, 0)))
        print ("Day 2: total distinct arrangements = {}".format(quickCountValid(datalist)))

# Main body
if __name__ == '__main__' :
    main()

