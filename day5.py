#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 5.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# Instance functions for specific problems

# Use BSP to get a unique seat code
def getSeatId(bspCode) :
    front = 0
    back = 127
    left = 0
    right = 7

    for c in bspCode :
        if c == 'F' :
            back = int((front + back) / 2)
        elif c == 'B' :
            front = int((front + back + 1) / 2)
        elif c == 'L' :
            right = int((right + left) / 2)
        elif c == 'R' :
            left = int((right + left + 1) / 2)
        else :
            print("getSeatId: bad BSP code!")
        #print ("f: {}, b: {}, l: {}, r: {}".format(front, back, left,right))

    return front * 8 + left

# Common helper functions
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            datalist.append(line.strip())

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day5.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    if args[1] == "1" :
        highest = 0
        for bspCode in datalist :
            highest = max(highest, getSeatId(bspCode))
        print ("Highest seat ID (part 1) is {}.".format(highest))
    elif args[1] == "2" :
        allSeats = []
        for bspCode in datalist :
            allSeats.append(getSeatId(bspCode))
        allSeats.sort()
        lastSeatId = 0
        for seatId in allSeats :
            if seatId - lastSeatId == 2 :
                print("My seat ID (part 2) is {}.".format(seatId - 1))
                return
            lastSeatId = seatId
        print ("Part 2 - no seat found!")


# Main body
if __name__ == '__main__' :
    main()

