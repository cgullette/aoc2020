#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 16.
"""

# Imports
import sys
import re

# Global variables
fieldRanges = dict()
yourTicket = []
nearbyTickets = []

def readData(day, test) :
    # data files should always be "dayX.txt" or "testX.txt"
    header = "day"
    if test is True:
        header = "test"
    filename = "{}{}.txt".format(header, day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            # Store each data element in list of ints
            thisline = []
            for item in line.strip().split(",") :
                thisline.append(int(item))
            datalist.append(thisline)

def main() :
    day = 14
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day{}.py part [test]'.format(day))
        print('  part == 1 or 2, "test" to use test data')
        sys.exit(1)

    if sys.argc > 2 and args[2] == "test" :
        readData(day, True)
    else :
        readData(day, False)

    # CALL YOUR INSTANCE FUNCTION HERE
    if args[1] == "1" :
        print("1") # call part 1
    elif args[1] == "2" :
        print("2") # call part 2

# Main body
if __name__ == '__main__' :
    main()

