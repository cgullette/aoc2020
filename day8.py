#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 8.
"""

# Imports
import sys
import re

# Global variables
datalist = []


# Read each instruction and store as a list
# [oper, arg, execCount = 0]
def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            line = line.strip().split()
            line[1] = int(line[1])
            line.append(0)
            datalist.append(line)


# Execute the current instruction pointed to by ctr
def execute(ctr,acc) :
    # If we've finished, return -1
    if (ctr >= len(datalist)) :
        print ("Program complete!")
        return -1, ctr, acc

    oper = datalist[ctr][0]
    arg = datalist[ctr][1]

    # If we've already run this instruction, return -2
    if datalist[ctr][2] > 0 :
        print("We've already executed {} {}, ABORT!".format(oper,arg))
        return -2, ctr, acc

    #print ("Executing {} {}, ctr = {}, acc = {}".format(oper,arg,ctr,acc))
    datalist[ctr][2] = 1

    if oper == "nop" :
        ctr += 1
    elif oper == "acc" :
        acc += arg
        ctr += 1
    elif oper == "jmp" :
        ctr += arg
    else :
        print ("BAD OPERATOR!")
        return -3, ctr, acc

    return 0, ctr, acc

def findBadOp() :
    ctr = 0
    acc = 0
    error = 0

    while error == 0 :
        error, ctr, acc = execute(ctr,acc)
    print("Day 1: final accumulator value is {}".format(acc))


def resetExecCounts() :
    for item in datalist : 
        item[2] = 0

def fixBadOp() :
    ctr = 0
    acc = 0
    error = 0

    for item in datalist :
        if item[0] == "nop" :
            if item[1] != 0 :
                item[0] = "jmp"
                while error == 0 :
                    error, ctr, acc = execute(ctr, acc)
                item[0] = "nop"
        elif item[0] == "jmp" :
            item[0] = "nop"
            while error == 0 :
                error, ctr, acc = execute(ctr, acc)
            item[0] = "jmp"

        if error == -1 :
            print("Day 2: complete run, acc is {}".format(acc))
            return

        resetExecCounts()
        ctr = acc = error = 0


def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day8.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    if args[1] == "1" :
        findBadOp()
    elif args[1] == "2" :
        fixBadOp()

# Main body
if __name__ == '__main__' :
    main()

