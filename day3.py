#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 3.
"""

# Imports
import sys

# Global variables
datalist = []

# Instance functions for specific problems

# Common helper functions
def readData(day):
    # data files should always be "dayX.txt"
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f :
            datalist.append(line.rstrip())

def getTree(lineNo, index) :
    if datalist[lineNo][index] == r'#' :
        return 1
    else :
        return 0

def getTreeCount(right, down) :
    treeCount = 0
    charIdx = right % len(datalist[0])
    lineIdx = down

    while lineIdx < len(datalist) :
        treeCount += getTree(lineIdx, charIdx)
        charIdx = (charIdx + right) % len(datalist[0])
        lineIdx += down

    return treeCount

def main():
    args = sys.argv[1:]

    if not args:
        print('usage: python3 day3.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    print(datalist)

    # CALL YOUR INSTANCE FUNCTION HERE
    if args[1] == "1":
        print("Treecount is {}".format(getTreeCount(3,1)))
    elif args[1] == "2":
        total = getTreeCount(1,1) * getTreeCount(3,1) * getTreeCount(5,1) * getTreeCount(7,1) * getTreeCount(1,2)
        print("Treecount product is {}".format(total))

# Main body
if __name__ == '__main__':
    main()

