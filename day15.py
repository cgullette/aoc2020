#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 1.
"""

# Imports
import sys
import re

# Global variables
#datalist = [0, 3, 6] # test data, part 1 answer is 436, part 2 is 175594
datalist = [14, 3, 1, 0, 9, 5] # problem data


# Speak a number. We track the last two turns this number
# has been spoken as a tuple value (and the number is the key).
def speakNumber(number, turn, numDict) :
    if number not in numDict :
        numDict[number] = (turn, 0)
    else :
        lastTurn = numDict[number][0]
        numDict[number] = (turn, lastTurn)

# Consider the last number spoken (at turn) and return the number to
# be spoken at turn + 1.
# If number has been spoken 0-1 times, return 0.
# If more than once, return the age (difference between last two times).
def considerLastNumber(number, turn, numDict) :
    # First check the map. If number is not a key, return 0
    if number not in numDict :
        return 0
    else :
        lastTurn = numDict[number][0]
        turnBeforeThat = numDict[number][1]
        if turnBeforeThat == 0 :
            return 0
        else :
            return lastTurn - turnBeforeThat


# Read the list first, then consider each number until we've gone through
# the provided number.
def playTheGame(number) :
    turn = 1
    numDict = dict()
    lastNumber = 0
    nextNumber = 0
    while turn <= number :
        if turn % 100000 == 0 :
            print("TURN = {}".format(turn))
        # First read the list
        if turn <= len(datalist) :
            lastNumber = datalist[turn-1]
        else :
            lastNumber = considerLastNumber(lastNumber, turn, numDict)
        speakNumber(lastNumber, turn, numDict)
        #print("turn {} is {}".format(turn, lastNumber))
        turn += 1
    print("The number for turn {} is {}.".format(turn-1, lastNumber))


def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 dayX.py [day] [part]')
        sys.exit(1)

    # CALL YOUR INSTANCE FUNCTION HERE
    if args[1] == "1" :
        playTheGame(2020)
    elif args[1] == "2" :
        # This is a little slow but it works
        playTheGame(30000000)

# Main body
if __name__ == '__main__' :
    main()

