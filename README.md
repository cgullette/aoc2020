# README #

This repo contains my solutions for the 2020 Advent of Code, written using Python 3.6.

### What is this repository for? ###

* So as I told you, this repo contains my solutions for the 2020 Advent of Code.

### What do I do? ###

* Install Python 3.6 or later
* Pull this repo
* Execute part 1 or 2 of each day with *python3 day<day>.py <day> <part>*
* e.g. For day 3 part 2, try *python3 day3.py 3 2*
* Some days contain test data files too, you'll have to edit the readData function for that day to load test<day>.txt

### What is Advent of Code ###

https://adventofcode.com

