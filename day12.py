#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 12.
"""

# Imports
import sys
import re

# Global variables
datalist = []


# Day 1: apply a movement instruction (ship only) and return the new location
# heading, east and north are ints describing ship's position and heading
# instruction[0] is an action and the rest is a single numeric argument
# Assumptions: The ship's heading is only ever E (0), S (90), W (180) or N (270)
def moveShip(instruction, heading, east, north) :
    action = instruction[:1]
    argument = int(instruction[1:])

    # Action F : move argument steps forward (NSEW)
    # Let's handle this first and convert the action to the correct direction
    if action == 'F' :
        if heading == 0 :
            action = 'E'
        elif heading == 90 :
            action = 'S'
        elif heading == 180 :
            action = 'W'
        elif heading == 270 :
            action = 'N'

    # The rest of possible instructions are [NSEWLR]
    # NSEW - move argument steps in the given direction
    # LR - turn argument degrees in the given direction
    if action == 'N' :
        north += argument
    elif action == 'S' :
        north -= argument
    elif action == 'E' :
        east += argument
    elif action == 'W' :
        east -= argument
    elif action == 'L' :
        heading = (heading - argument) % 360
    elif action == 'R' :
        heading = (heading + argument) % 360

    # Return the new state as a tuple
    return heading, east, north


# Day 2: apply a movement instruction (ship or waypoint) and return the new location
# Ship position is absolute (shipE, shipN), waypoint position is relative to ship
# instruction[0] is an action and the rest is a single numeric argument
# Assumptions: The ship's heading is only ever E (0), S (90), W (180) or N (270)
def moveShipOrWaypoint(instruction, shipE, shipN, waypointE, waypointN) :
    action = instruction[:1]
    argument = int(instruction[1:])

    # The rest of possible instructions are [NSEWLR]
    # NSEW : move waypoint by argument steps in the given direction
    # LR : rotate waypoint around the ship by argument degrees in the given direction
    # F : move ship toward the waypoint argument times
    if action == 'N' :
        waypointN += argument
    elif action == 'S' :
        waypointN -= argument
    elif action == 'E' :
        waypointE += argument
    elif action == 'W' :
        waypointE -= argument
    elif action == 'L' :
        if argument == 90 :
            waypointE, waypointN = -waypointN, waypointE
        elif argument == 180 :
            waypointE, waypointN = -waypointE, -waypointN
        elif argument == 270 :
            waypointE, waypointN = waypointN, -waypointE
    elif action == 'R' :
        if argument == 90 :
            waypointE, waypointN = waypointN, -waypointE
        elif argument == 180 :
            waypointE, waypointN = -waypointE, -waypointN
        elif argument == 270 :
            waypointE, waypointN = -waypointN, waypointE
    elif action == 'F' :
        for i in range(argument) :
            shipE += waypointE
            shipN += waypointN

    # Return the new state as a tuple
    return shipE, shipN, waypointE, waypointN


def readData(day) :
    # data files should always be "dayX.txt"
    #filename = "test{}.txt".format(day)
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            datalist.append(line.strip())


def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 dayX.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    heading = 0
    shipE = 0
    shipN = 0
    waypointE = 10
    waypointN = 1

    if args[1] == "1" :
        for instruction in datalist :
            heading, shipE, shipN = moveShip(instruction, heading, shipE, shipN)
        print("Day 1: final heading = {}, shipE = {}, shipN = {}".format(heading, shipE, shipN))
        print("Day 1: final Manhattan distance is {}".format(abs(shipE) + abs(shipN)))
    elif args[1] == "2" :
        for instruction in datalist :
            shipE, shipN, waypointE, waypointN = moveShipOrWaypoint(instruction, shipE, shipN, waypointE, waypointN)
        print("Day 2: final shipE = {}, shipN = {}".format(shipE, shipN))
        print("Day 2: final Manhattan distance is {}".format(abs(shipE) + abs(shipN)))


# Main body
if __name__ == '__main__' :
    main()

