#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 4.
"""

# Imports
import sys
import re

# Global variables
datalist = []

# Given a line iterator into the datalist, pull off all lines
# of a single passport and return validity of passport (bool)
def getPassport(lineIter, day) : 
    passport = dict()
    line = next(lineIter)
    while (len(line) > 0) :
        passport.update((x.strip(),y.strip()) for x, y in (entry.split(':') for entry in line.split()))
        line = next(lineIter)
    if (day == 1) :
        return checkDay1PP(passport)
    elif (day == 2) :
        return checkDay2PP(passport)
    else :
        return True

def checkDay1PP(passport) :
    # Check for all non-optional keys
    return ("byr" in passport and "iyr" in passport and "eyr" in passport
        and "hgt" in passport and "hcl" in passport and "ecl" in passport and "pid" in passport)

# cid (Country ID) - ignored, missing or not. 
def checkDay2PP(passport) :
    # First do a day 1 check
    if not checkDay1PP(passport) :
        return False

    # byr (Birth Year) - four digits; at least 1920 and at most 2002.
    temp = int(passport["byr"])
    if (temp < 1920 or temp > 2002) :
        return False
    # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    temp = int(passport["iyr"])
    if (temp < 2010 or temp > 2020) :
        return False
    # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    temp = int(passport["eyr"])
    if (temp < 2020 or temp > 2030) :
        return False

    # hgt (Height) - a number followed by either cm or in:
        # If cm, the number must be at least 150 and at most 193.
        # If in, the number must be at least 59 and at most 76.
    match = re.match(r"([0-9]+)(\w+)", passport["hgt"])
    if not match :
        return False
    if match.group(2) == "cm" :
        if int(match.group(1)) < 150 or int(match.group(1)) > 193 :
            return False
    elif match.group(2) == "in" :
        if int(match.group(1)) < 59 or int(match.group(1)) > 76 :
            return False
    else :
        return False

    # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    match = re.match(r"#[0-9a-f]{6}$", passport["hcl"])
    if not match :
        return False

    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    match = re.match(r"amb|blu|brn|gry|grn|hzl|oth$", passport["ecl"])
    if not match :
        return False

    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    match = re.match(r"[0-9]{9}$", passport["pid"])
    if not match :
        return False
    
    return True

# Call getPassport until we run out of file. Note that I "cheated" and added a blank line to the end
# of the data file in order to avoid special case handling for the last passport
def countValidPassports(day) :
    count = 0
    lineIter = iter(datalist)

    try :
        while True :
            if getPassport(lineIter, day) :
                count += 1
    except StopIteration as e :
        return count

# Common helper functions
def readData(day) :
    # data files should always be "dayX.txt"
    filename = "day{}.txt".format(day)
    #filename = "test{}.txt".format(day)

    # Open the file and store each line
    with open(filename, "r", encoding="utf8") as f :
        for line in f:
            datalist.append(line.rstrip())

def main() :
    args = sys.argv[1:]

    if not args :
        print('usage: python3 day4.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    print ("Day {}: Number of valid passports is {}.".format(int(args[1]), countValidPassports(int(args[1]))))

# Main body
if __name__ == '__main__' :
    main()

