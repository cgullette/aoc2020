#==============================================================================
# Copyright © 2020 Chris Gullette
# All Rights Reserved.
#==============================================================================

#!/user/bin/env python3

"""
Advent of Code 2020: Day 1.
"""

# Imports
import sys

# Global variables
datalist = []

# Instance functions for specific problems

def day1part1():
    # Sort the datalist
    datalist.sort()
    # Track top and bottom, adding and moving until we find two values that add to 2020
    top = 0
    bottom = len(datalist) - 1
    x = datalist[top][0]
    z = datalist[bottom][0]
    done = x + z
    while done != 2020:
        if done > 2020:
            bottom -= 1
            z = datalist[bottom][0]
        else:
            top += 1
            x = datalist[top][0]
        done = x + z

    a = datalist[top][0]
    print("{} + {} = {}, {} * {} = {}".format(x, z, done, x, z, x*z))

def day1part2():
    # Sort the datalist
    datalist.sort()
    # Track top, middle and bottom, adding and moving until we find three values that add to 2020
    top = 0
    middle = 1
    bottom = len(datalist) - 1
    x = datalist[top][0]
    y = datalist[middle][0]
    z = datalist[bottom][0]
    done = x + y + z

    # If too big, move bottom up and reset middle
    # If middle hits bottom, move top down and reset middle
    # If too small, move middle down
    while done != 2020:
        if done > 2020:
            bottom -= 1
            middle = top + 1
        elif middle == bottom - 1:
            top += 1
            middle = top + 1
        else:
            middle += 1
        x = datalist[top][0]
        y = datalist[middle][0]
        z = datalist[bottom][0]
        done = x + y + z

    print("{}, {}, {}: sum = {}, product = {}".format(x, y, z, done, x*y*z))

# Common helper functions
def readData(day):
    # data files should always be "dayX.txt"
    filename = "day{}.txt".format(day)

    # Open the file
    with open(filename, "r", encoding="utf8") as f:
        for line in f:
            # Store each data element in an array (probably ints)
            thisline = []
            for item in line.rstrip().split(","):
                thisline.append(int(item))
            datalist.append(thisline)

def main():
    args = sys.argv[1:]

    if not args:
        print('usage: python3 day1.py [day] [part]')
        sys.exit(1)

    readData(args[0])
    #print(datalist)

    if args[1] == "1":
        day1part1()
    elif args[1] == "2":
        day1part2()

# Main body
if __name__ == '__main__':
    main()

